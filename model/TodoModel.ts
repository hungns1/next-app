export interface TodoModel {
    id: string
    title: string
    complete: boolean
}