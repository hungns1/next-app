import Image from 'next/image';
import Link from 'next/link';
import {TodoItem} from '../components'
import prisma from './db';
function getTodo () {
  return prisma.todo.findMany();
}

export default async function Home() {
  const data = await getTodo()
  console.log(data);
//  await prisma.todo.create({data:{title:'todo one', complete:false}})
  return (
    <div>
      <div className="text-center flex justify-between items-center">
        <h1 className='font-bold text-red-300 text-2xl'>Todo Tutorial</h1>
        <Link className='rounded-lg px-4 py-2 border-2 border-blue-500 text-blue-500 hover:bg-blue-600 hover:text-blue-100 duration-300' href={'new'}>Create New</Link>
      </div>
      <div >
          {data.map(item => (
           <TodoItem key={item.id} {...item}/>
        ))}
       
      </div>
      
    </div>
  );
}
