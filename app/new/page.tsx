import Link from 'next/link';
import { redirect } from "next/navigation";
import prisma from '../db.ts';
async function onCreateTodo(data: FormData) {
  "use server"
  const title = data.get("title").valueOf()
  const complete = data.get("complete")
  
  if (typeof title !== 'string' || title.length === 0) {
      throw new Error("Looxi");
      
  }else{
    await prisma.todo.create({data: {title, complete:complete !== null}})
    redirect('/')
  }
    
}

export default function CreateNew() {
  return (
    <>
    <h1 className='font-bold text-red-300 text-2xl text-center'>Create New</h1>
      <div>
        <form action={onCreateTodo}>
          <div className="flex flex-col gap-6">
            <div className="flex gap-2">
              <label htmlFor="title">Title</label>
              <input type="text" name="title" className="h-[30px]" />
            </div>
            <div className="flex gap-2">
              <label htmlFor="title">Complete</label>
              <input type="checkbox" name="complete"/>
            </div>

            <div className="flex justify-center gap-4">
              <button
                type="submit"
                className="rounded-lg px-4 py-2 border-2 border-gray-900 text-gray-900 hover:bg-gray-900 hover:text-gray-100 duration-300"
              >
                Submit
              </button>
              <Link
                className="rounded-lg px-4 py-2 border-2 border-blue-500 text-blue-500 hover:bg-blue-600 hover:text-blue-100 duration-300"
                href="/"
              >
                Back
              </Link>
            </div>
          </div>
        </form>
      </div>
    </>
  );
}
