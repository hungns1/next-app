import { TodoModel } from "../model";
export function TodoItem(data : TodoModel) {
  return (
    <>
      <div className="flex justify-start mt-8 gap-2">
        <input id={data.id} type="checkbox" className="cursor-pointer peer" defaultChecked={data.complete}/>
        <label className="text-lg font-semibold cursor-pointer peer-check:line-through" htmlFor={data.id}>
          {data.title}
        </label>
      </div>
    </>
  );
}
